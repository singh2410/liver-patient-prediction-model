#!/usr/bin/env python
# coding: utf-8

# # Liver Patient Prediction Model
# #By- Aarush Kumar
# #Dated: August 01,2021

# In[1]:


from IPython.display import Image
Image(url='https://media.istockphoto.com/photos/doctor-shows-liver-in-hand-picture-id899731774?k=6&m=899731774&s=612x612&w=0&h=VdYLA6MQXoaXZ6zO4X8jCHYxFQlZ1-Ho8mPnV4N2leg=')


# In[2]:


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import warnings
warnings.simplefilter("ignore")


# In[3]:


df = pd.read_csv("/home/aarush100616/Downloads/Projects/Liver Patient Prediction/indian_liver_patient.csv")
df.head()


# In[4]:


# Looking for missing values in the dataset
df.isnull().sum()


# In[5]:


df.shape


# In[6]:


df.dtypes


# In[7]:


df.describe()


# In[8]:


df.size


# In[9]:


# Re-naming the columns
df.rename(columns={'Dataset':'Outcome'}, inplace=True)
df.info()


# In[10]:


df['Gender'] = df['Gender'].map({'Male': 1, 'Female': 2})


# In[11]:


df.head()


# In[12]:


# Dropping the missing values
df = df.dropna()


# In[13]:


# Having a look at the correlation matrix
fig, ax = plt.subplots(figsize=(8,6))
sns.heatmap(df.corr(), annot=True, fmt='.1g', cmap="viridis", cbar=False);


# In[14]:


print ('Total Unhealthy Livers : {} '.format(df.Outcome.value_counts()[1]))
print ('Total Healthy Livers : {} '.format(df.Outcome.value_counts()[2]))


# In[15]:


plt.style.use("seaborn")
fig, ax = plt.subplots(figsize=(7,7))

plt.pie(x=df["Outcome"].value_counts(), 
        colors=["firebrick","seagreen"], 
        labels=["UnHealthy Liver","Healthy Liver"], 
        shadow = True, 
        explode = (0, 0.1)
        )

plt.show()


# In[16]:


df.Gender.value_counts()


# In[17]:


plt.style.use("seaborn")
fig, ax = plt.subplots(figsize=(7,7))
plt.pie(x=df["Gender"].value_counts(), 
        colors=["skyblue","pink"], 
        labels=["Male","Female"], 
        shadow = True, 
        autopct="%1.2f%%", 
        explode = (0, 0.1)
        )
plt.show()


# In[18]:


plt.style.use("seaborn")
fig, ax = plt.subplots(figsize=(8,6))
sns.histplot(x=df["Age"], kde=True, color="seagreen");


# In[19]:


fig, ax =plt.subplots(4,2, figsize=(20,25)) 
plt.style.use("seaborn")
sns.histplot(x = df["Total_Bilirubin"], hue = df["Outcome"], palette="viridis", kde=True, ax=ax[0,0]);
ax[0,0].set_xlabel("Total_Bilirubin",fontsize=15)
sns.histplot(x = df["Direct_Bilirubin"], hue = df["Outcome"], palette="viridis", kde=True, ax=ax[0,1]);
ax[0,1].set_xlabel("Direct_Bilirubin",fontsize=15)
sns.histplot(x = df["Alkaline_Phosphotase"], hue = df["Outcome"], palette="dark", kde=True, ax=ax[1,0]);
ax[1,0].set_xlabel("Alkaline_Phosphotase",fontsize=15)
sns.histplot(x = df["Alamine_Aminotransferase"], hue = df["Outcome"], palette="dark", kde=True, ax=ax[1,1]);
ax[1,1].set_xlabel("Alamine_Aminotransferase",fontsize=15)
sns.histplot(x = df["Aspartate_Aminotransferase"], hue = df["Outcome"], palette="flare", kde=True, ax=ax[2,0]);
ax[2,0].set_xlabel("Aspartate_Aminotransferase",fontsize=15)
sns.histplot(x = df["Total_Protiens"], hue = df["Outcome"], palette="flare", kde=True, ax=ax[2,1]);
ax[2,1].set_xlabel("Total_Protiens",fontsize=15)
sns.histplot(x = df["Albumin"], hue = df["Outcome"], palette="viridis", kde=True, ax=ax[3,0]);
ax[3,0].set_xlabel("Albumin",fontsize=15)
sns.histplot(x = df["Albumin_and_Globulin_Ratio"], hue = df["Outcome"], palette="viridis", kde=True, ax=ax[3,1]);
ax[3,1].set_xlabel("Albumin_and_Globulin_Ratio",fontsize=15);


# In[20]:


plt.style.use("seaborn")
fig, ax = plt.subplots(figsize=(8,6))
sns.histplot(x = df["Age"], hue = df["Outcome"], palette="spring", kde=True);


# ### Splitting the data into training and test datasets

# In[21]:


# X data
X = df.drop("Outcome", axis=1)
X.head()


# In[22]:


# y data
y = df["Outcome"]
y.head()


# In[23]:


from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)


# In[24]:


len(X_train), len(X_test)


# In[25]:


# Scaling the data 
from sklearn.preprocessing import StandardScaler
scaler = StandardScaler()
X_train = scaler.fit_transform(X_train)
X_test = scaler.transform(X_test)


# ### Logistic Regression

# In[26]:


from sklearn.linear_model import LogisticRegression
lr = LogisticRegression()
lr.fit(X_train, y_train)


# In[27]:


LogisticRegressionScore = lr.score(X_test, y_test)
print("Accuracy obtained by Logistic Regression model:",LogisticRegressionScore*100)


# In[28]:


# Having a look at the confusion matrix
from sklearn.metrics import confusion_matrix, classification_report
y_pred_lr = lr.predict(X_test)
cf_matrix = confusion_matrix(y_test, y_pred_lr)
sns.heatmap(cf_matrix, annot=True, cmap="Spectral")
plt.title("Confusion Matrix for Logistic Regression", fontsize=14, fontname="Helvetica", y=1.03);


# In[29]:


# Having a look at the classification report of Logistic Regression
from sklearn import metrics
print(metrics.classification_report(y_test, y_pred_lr))


# ## Random Forest Classifier

# In[30]:


from sklearn.ensemble import RandomForestClassifier
rfc = RandomForestClassifier(n_estimators = 100)
rfc.fit(X_train,y_train)


# In[31]:


RandomForestClassifierScore = rfc.score(X_test, y_test)
print("Accuracy obtained by Random Forest Classifier model:",RandomForestClassifierScore*100)


# In[32]:


# Having a look at the confusion matrix
y_pred_rfc = rfc.predict(X_test)
cf_matrix = confusion_matrix(y_test, y_pred_rfc)
sns.heatmap(cf_matrix, annot=True, cmap="Spectral")
plt.title("Confusion Matrix for Random Forest Classifier", fontsize=14, fontname="Helvetica", y=1.03);


# In[33]:


# Classification report of Random Forest Classifier
print(metrics.classification_report(y_test, y_pred_rfc))


# ### K Neighbors Classifier

# In[34]:


from sklearn.neighbors import KNeighborsClassifier
knn = KNeighborsClassifier(4)
knn.fit(X_train,y_train)


# In[35]:


KNeighborsClassifierScore = knn.score(X_test, y_test)
print("Accuracy obtained by K Neighbors Classifier model:",KNeighborsClassifierScore*100)


# In[36]:


# Having a look at the confusion matrix
y_pred_knn = knn.predict(X_test)
cf_matrix = confusion_matrix(y_test, y_pred_knn)
sns.heatmap(cf_matrix, annot=True, cmap="Spectral")
plt.title("Confusion Matrix for K Neighbors Classifier", fontsize=14, fontname="Helvetica", y=1.03);


# In[37]:


# Classification report of K Neighbors Classifier
print(metrics.classification_report(y_test, y_pred_knn))


# ## DecisionTreeClassifier

# In[38]:


from sklearn.tree import DecisionTreeClassifier
dtc = DecisionTreeClassifier()
dtc.fit(X_train, y_train)


# In[39]:


DecisionTreeClassifierScore = dtc.score(X_test,y_test)
print("Accuracy obtained by Decision Tree Classifier model:",DecisionTreeClassifierScore*100)


# In[40]:


# Confusion matrix
y_pred_dtc = dtc.predict(X_test)
cf_matrix = confusion_matrix(y_test, y_pred_dtc)
sns.heatmap(cf_matrix, annot=True, cmap="Spectral")
plt.title("Confusion Matrix for Decision Tree Classifier", fontsize=14, fontname="Helvetica", y=1.03);


# In[41]:


# Classification Report of Decision Tree Classifier
print(metrics.classification_report(y_test, y_pred_dtc))


# ## CatBoost Classifier

# In[47]:


from catboost import CatBoostClassifier
cat = CatBoostClassifier(iterations=10)
cat.fit(X_train, y_train);


# In[48]:


CatBoostClassifierScore = cat.score(X_test,y_test)
print("Accuracy obtained by CatBoost Classifier model:",CatBoostClassifierScore*100)


# In[49]:


# Confusion matrix
y_pred_cat = cat.predict(X_test)
cf_matrix = confusion_matrix(y_test, y_pred_cat)
sns.heatmap(cf_matrix, annot=True, cmap="Spectral")
plt.title("Confusion Matrix for CatBoost Classifier", fontsize=14, fontname="Helvetica", y=1.03);


# In[50]:


# Classification Report of CatBoost Classifier
print(metrics.classification_report(y_test, y_pred_cat))


# ## Gradient Boosting Classifier

# In[42]:


from sklearn.ensemble import GradientBoostingClassifier
gb = GradientBoostingClassifier()
gb.fit(X_train, y_train)


# In[43]:


GradientBoostingClassifierScore = gb.score(X_test,y_test)
print("Accuracy obtained by Gradient Boosting Classifier model:",GradientBoostingClassifierScore*100)


# In[44]:


# Confusion matrix
y_pred_gb = gb.predict(X_test)
cf_matrix = confusion_matrix(y_test, y_pred_gb)
sns.heatmap(cf_matrix, annot=True, cmap="Spectral")
plt.title("Confusion Matrix for Gradient Boosting Classifier", fontsize=14, fontname="Helvetica", y=1.03);


# In[45]:


# Classification Report of Gradient Boosting Classifier
print(metrics.classification_report(y_test, y_pred_gb))


# In[51]:


plt.style.use("seaborn")

x = ["LogisticRegression", 
     "Decision Tree Classifier", 
     "RandomForestClassifier", 
     "KNeighborsClassifier", 
     "CatBoost Classifier", 
     "Gradient Boosting Classifier"]

y = [LogisticRegressionScore, 
     DecisionTreeClassifierScore, 
     RandomForestClassifierScore, 
     KNeighborsClassifierScore, 
     CatBoostClassifierScore, 
     GradientBoostingClassifierScore]

fig, ax = plt.subplots(figsize=(8,6))
sns.barplot(x=x,y=y, palette="crest");
plt.ylabel("Model Accuracy")
plt.xticks(rotation=40)
plt.title("Model Comparison - Model Accuracy", fontsize=14, fontname="Helvetica", y=1.03);


# ## Hyperparameter Tuning on Logistic Regression¶

# In[52]:


from sklearn.model_selection import GridSearchCV

param_grid = [
    {
      "penalty": ["l1", "l2", "elastic", "none" ],
      "C" : np.logspace(-4, 4, 20),
      "solver" : ["sag", "saga", "lbfgs", "liblinear", "newton-cg"],
      "max_iter" : [100, 1000, 2500, 5000]
    }
]

grid_search_lr = GridSearchCV(estimator = lr, 
                              param_grid = param_grid, 
                              cv = 5, 
                              n_jobs = -1, 
                              verbose = True)


# In[53]:


grid_search_lr.fit(X_train, y_train)


# In[54]:


grid_search_lr.best_params_


# In[55]:


grid_search_lr.best_score_


# In[56]:


grid_search_lr_predict = grid_search_lr.predict(X_test)


# In[57]:


print('Improvement in Logistic Regression after GridSearchCV: {:0.2f}%.'.format(100 * (grid_search_lr.best_score_ - LogisticRegressionScore) / LogisticRegressionScore))


# In[58]:


# Comparing the results after the improvement in Logistic Regression

plt.style.use("seaborn")

x = ["Logistic Regression",
     "GridSearch-LogisticRegression"]

y = [LogisticRegressionScore,
     grid_search_lr.best_score_]

fig, ax = plt.subplots(figsize=(7,7))
sns.barplot(x=x,y=y, palette="viridis");
plt.ylabel("Accuracy")
plt.xticks(rotation=30)
plt.title("LogisticRegression  vs  GridSearched LogisticRegression", fontsize=14, fontname="Helvetica", y=1.03);


# In[59]:


# Comparing the GridSearch-Logistic Regression and Gradient Boosting Classifier 

plt.style.use("seaborn")

x = ["Gradient Boosting Classifier",
     "GridSearch-LogisticRegression"]

y = [GradientBoostingClassifierScore,
     grid_search_lr.best_score_]

fig, ax = plt.subplots(figsize=(7,7))
sns.barplot(x=x,y=y, palette="viridis");
plt.ylabel("Accuracy")
plt.xticks(rotation=30)
plt.title("Gradient Boosting Classifier  vs  GridSearched LogisticRegression", fontsize=14, fontname="Helvetica", y=1.03);


# In[60]:


# Classification Report of GridSearch-LogisticRegression
print(classification_report(y_test, grid_search_lr_predict))

